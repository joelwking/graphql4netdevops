# graphql4netdevops

**Introduction to GraphQL for NetDevOps Engineers**: this repository is a companion to the Meetup topic presented at the [programmability-and-automation-meetup-group](https://www.meetup.com/pro/programmability-and-automation-meetup-group/admin/chapters).

There is also a live stream of the presentation on [youtube.](https://www.youtube.com/watch?v=uhA69Vcu_vU&list=PLJi2T8I3T8CLbns3eUujwZlzlMSyxHq43)
## Abstract

GraphQL is a query language for APIs and has been adopted by popular web services, including GitHub, ArangoDB and Nautobot, a network automation platform. This session introduces GraphQL and demonstrates using Nautobot as a Source of Truth for managing the configuration of Arista Containerized EOS (cEOS) routers using Ansible.

Network engineers who wish to learn GraphQL for Nautobot, Ansible, and cEOS will benefit from this session.

## Adopters

Recently Meetup.com added GraphQL API functionality. The [benefits of GraphQL](https://www.meetup.com/api/general/#graphQl-introduction) are best described in the introduction:

> *GraphQL is a modern way to request data from APIs. An alternative to REST implementations, it was developed to minimize the amount of data transferred and increase software engineer productivity.*

> *A REST API returns all fields and data it was set up to deliver, even if you are not using them. GraphQL allows you to craft your own queries and returns the exact fields you specify.*

Adopters of GraphQL include:

* [Coursera](https://www.graphql.com/articles/coursera-graphql), 
* [GitHub](https://docs.github.com/en/graphql), 
* [Pinterest](https://www.pinterest.com/pin/195202965082795823/), 
* [ArangoDB](https://www.arangodb.com/2016/02/using-graphql-nosql-database-arangodb/)

Nautobot is a Network Source of Truth and Network Automation Platform for network operators which [leverages GraphQL](https://blog.networktocode.com/post/leveraging-the-power-of-graphql-with-nautobot/).

## Technical components

This repository is structured to execute as a VSCode development container as the Ansible control node with Nautobot and containerized EOS running as containers in a cloud instance.

### Creating the lab environment

Instructions for creating the lab environment are in `LAB_BUILD.md`.

To invoke the development container, familiarize yourself with the concepts of [Instructions for using VS Code Remote Explorer for a container](https://gitlab.com/joelwking/vs_code_remote_container). 

Clone this repository, enter the clone directory, and then launch VSCode.

```shell
git clone https://gitlab.com/joelwking/graphql4netdevops
cd graphql4netdevops
code .
```

### Lab ports and addressing

Modify these files for your cloud instance and ports.

* The values in `playbooks/group_vars/all.yml` for variable `lab_instance` specify the hostname or IP address of the cloud instance running the lab containers. 
* Verify the port number `url: "http://{{ lab_instance }}:8001"` for the Nautobot installation in `playbooks/group_vars/all.yml`

### Credentials

You will need to login Nautobot and create a token. The username and password for the cEOS containers are configured as part of the `LAB_BUILD.md` as is the username, password and port number of your Nautobot lab.

### Create Bearer Token

Create a Bearer Token at: `http://<your_instance_ip>:8001/admin/users/token/`. The token will need to be entered in `playbooks/files/vault.yml` on your development container (along with the cEOS username and password) to execute the playbooks.

### Update credential file

The file `playbooks/files/vault.yml` contains the cEOS userid and password, and the bearer token for Nautobot. Specify the values from your installation. Replace the version from the repository with the following example.

```yaml
#
# Encrypt this file with Ansible Vault
#
vault_arista:
  userid: admin
  password: NetCraftsmen!123

vault_nautobot:
  token: b20f349628dd3425842e9ee38d0c3ccdfe340e06
```

After modifying the userid, password and token values, you should encrypt this file with `ansible-vault encrypt vault.yml`.

### Create the framework

In order to utilize Nautobot as a Source of Truth, some baseline configuration must be implemented before adding the network devices (the routers, the containerized EOS).

#### Set up your environment

Activte the virtual environment and verify the Ansible configuration.

```shell
source /opt/ansible/bin/activate
cd playbooks
ansible --version
ansible 2.10.17
  config file = None
  configured module search path = ['/root/.ansible/plugins/modules', '/usr/share/ansible/plugins/modules']
  ansible python module location = /opt/ansible/lib/python3.8/site-packages/ansible
  executable location = /opt/ansible/bin/ansible
  python version = 3.8.10 (default, Jun 23 2021, 15:28:49) [GCC 8.3.0]
```

By invoking the `build_nautobot.yml` playbook, we populate the Regions, Sites, Tenants, Manufacturers, Device Types and Roles, VLANS, VRFs, Rack groups, Rack roles and racks, IPAM roles and network prefixes. Log on the web interface of Nautobot and view the 'before' and 'after' the playbook executes.

The configuration values for this step are specified in `files/global.yml`. You may modify them as required.

```shell
 ansible-playbook ./build_nautobot.yml --ask-vault-password -i files/inventory.yml
```

>Note: to avoid having to type the vault password each time you execute the playbook, you can store it in a file.

```
echo mysecret >~/.vault_pass.txt
chmod 600 ~/.vault_pass.txt
export ANSIBLE_VAULT_PASSWORD_FILE=~/.vault_pass.txt
```

### Onboard the devices

Now that the baseline configuration has been applied to Nautobot, we can onboard the configuration of the containerized EOS routers. Their configuration is specified in `host_vars/ceos1.example.net.yml` and `host_vars/ceos2.example.net.yml`. The hostnames must match the values in `files/inventory.yml`.

```shell
ansible-playbook ./onboard_device.yml -e device_name=ceos1.example.net -i files/inventory.yml
```

>Note: to use the Network CLI connection method, also on-board the group `cli_ceos` in addition to `cloud_ceos`.

You can onboard all the hosts in the group `cloud_ceos` at once.

```shell
ansible-playbook ./onboard_device.yml -e 'device_name=cloud_ceos' -i files/inventory.yml
```

Or, specify the group name `cloud_ceos`, and limit it to two hosts.

```shell
ansible-playbook ./onboard_device.yml -e 'device_name=cloud_ceos' -i files/inventory.yml --limit ceos2.example.net,ceos1.example.net
```

The onboarding process defines the device, the interfaces and the IP addresses.

### Configure the devices

Now, finally to the use of **GraphQL**! Let's examine the playbook `configure_device.yml` and the GraphQL configuration `files/query_interfaces.graphql`.

Review the [Intro to GraphQL](https://graphql.org/learn/queries/) and the session recording on the WWT [community](https://www.wwt.com/community/programmability-and-automation-meetup-group) page.

>Note: When configuring devices, inventory group `cloud_ceos` uses the EAPI connection method, inventory group `cli_ceos` uses the connection method of network CLI. Group `cli_ceos` uses the DNS domain `example.org`. Group `cloud_ceos` uses the DNS domain `example.net`. Both domains refer to the same two switches, only the connection method changes.

Both the containerized EOS devices can be configured (using the EAPI) in playbook by using:

```shell
ansible-playbook ./configure_device.yml  -e group_name=cloud_ceos -f 1 -i files/inventory.yml --ask-vault-password
```

To use the connection method network CLI connection method:

```shell
ansible-playbook ./configure_device.yml  -e group_name=cli_ceos -f 1 -i files/inventory.yml --ask-vault-password
```

>Note: use the verbose switch `-v` and set `export ANSIBLE_STDOUT_CALLBACK=yaml` to view the Query String, Graph Variables and data returned.

```yaml
Query String:   query Interfaces($device_name: [String]){
  devices (name: $device_name ){
    id
    name
    interfaces {
      name
      label
      enabled
      mac_address
      description
      mgmt_only
      mtu
      ip_addresses {
        id
        dns_name
        address
        status {
          name
        }
      }
    }
  }
  }
Graph Variables: {'device_name': 'ceos2.example.net'}
ok: [ceos2.example.net] => changed=false 
  data:
    devices:
    - id: cc05c9e4-a1a8-4a3e-9817-128c1013f11a
      interfaces:
      - description: ''
        enabled: true
        ip_addresses:
        - address: 192.0.2.2/30
          dns_name: ceos2.example.net-Ethernet1
          id: 181d35d6-9313-4fcd-80b1-f0bac5ec078a
          status:
            name: Reserved
        label: default
        mac_address: null
        mgmt_only: false
        mtu: 9214
        name: Ethernet1
      - description: ''
        enabled: true
        ip_addresses:
        - address: 192.0.2.6/30
          dns_name: ceos2.example.net-Ethernet2
          id: 1415698c-82f9-400d-8758-cd3648308427
          status:
            name: Reserved
        label: default
        mac_address: null
        mgmt_only: false
        mtu: 1524
        name: Ethernet2
      - description: ''
        enabled: true
        ip_addresses:
        - address: 198.51.100.2/31
          dns_name: ceos2.example.net-Loopback0
          id: 2ea7051c-66a7-42c6-834f-306918f9497e
          status:
            name: Reserved
        label: default
        mac_address: null
        mgmt_only: false
        mtu: 9214
        name: Loopback0
      name: ceos2.example.net
  graph_variables:
    device_name: ceos2.example.net
  query: |2-
      query Interfaces($device_name: [String]){
      devices (name: $device_name ){
        id
        name
        interfaces {
          name
          label
          enabled
          mac_address
          description
          mgmt_only
          mtu
          ip_addresses {
            id
            dns_name
            address
            status {
              name
            }
          }
        }
      }
      }
```

## Using GraphQL as an inventory source
Nautobot can be used as an inventory source using GraphQL queries.

### Specify the location of the inventory plugin
Either update the [`ansible.cfg`](https://raw.githubusercontent.com/ansible/ansible/devel/examples/ansible.cfg) file (in the playbook directory) or specify the location of the inventory plugin using an environment variable.

> Note: Running `ansible-config dump --only-changed` in the playbook directory will show the changes to configuration file.

```shell
export ANSIBLE_INVENTORY_PLUGINS=~/.ansible/collections/ansible_collections/networktocode/nautobot/plugins/inventory
```

Verify Ansible can location the inventory plugin, and review the documentation, issue:

```shell
ansible-doc -t inventory gql_inventory
```

Specify the nautobot token as an environment variable or update `files/inventory_gql.yml` to specify the token.

```shell
export NAUTOBOT_TOKEN=579e1e417b4b6f1d9c9b1d24d52467b1db71ecc9
```

Verify the inventory source:

```shell
ansible-inventory -v --list -i files/inventory_gql.yml
```

Execute the sample playbook.

```shell
ansible-playbook graph_ql_as_inventory.yml -i files/inventory_gql.yml
```

## Author

Joel W. King @joelwking