
Installing cEOS and Nautobot on a public / private cloud instance
-----------------------------------------------------------------

## Launch instance

Launch an instance in your public or private cloud. These instructions are for `Ubuntu 18.04.6 LTS (GNU/Linux 5.4.0-1058-aws x86_64)` with 100GB storage, 8 vCPU, 32GB memory. In AWS EC2, this is a t2.xlarge instance.

In DigitalOcean, the droplet is Basic, regular SSD, 8 Virtual CPU `16 GB Memory / 320 GB Disk / NYC1 - Ubuntu 18.04 (LTS) x64`

The Security group inbound rules configured are:

| Port Range    | Protocol      | Source           |
|:------------- |:--------------| :----------------|
| 4431 - 4432   | TCP           | 198.51.100.106/32|
| 8000 - 8001   | TCP           | 198.51.100.106/32|
| 2201 - 2202   | TCP           | 198.51.100.106/32|
| 22            | TCP           | 198.51.100.106/32|

>Note: The source IP address can be 0.0.0.0/0, but using your Internet connection source IP address is more secure.

### Login

Login to the instance with the security key pair (or password) for your instance. Make a directory and enter it.

**AWS**
```shell
ssh ubuntu@<your_instance_ip> -i ~/.ssh/customer_training.pem
mkdir cEOS
cd cEOS
```

**Digital Ocean**
```shell
ssh root@<droplet_ip> -i ~/.ssh/id_ed25519
mkdir
```

### Install Docker

Install Docker Engine on Ubuntu, using these [instructions](https://docs.docker.com/engine/install/ubuntu/). For example:

```shell
sudo apt-get update
sudo apt-get install \
    ca-certificates \
    curl \
    gnupg \
    lsb-release
curl -fsSL https://download.docker.com/linux/ubuntu/gpg | sudo gpg --dearmor -o /usr/share/keyrings/docker-archive-keyring.gpg
echo \
  "deb [arch=$(dpkg --print-architecture) signed-by=/usr/share/keyrings/docker-archive-keyring.gpg] https://download.docker.com/linux/ubuntu \
  $(lsb_release -cs) stable" | sudo tee /etc/apt/sources.list.d/docker.list > /dev/null
sudo apt-get update
sudo apt-get install docker-ce docker-ce-cli containerd.io
sudo docker run hello-world
```

Follow these [instructions](https://docs.docker.com/engine/install/linux-postinstall/#manage-docker-as-a-non-root-user) to manage Docker from a non-root account.

```shell
sudo groupadd docker
sudo usermod -aG docker $USER
newgrp docker 
docker run hello-world
```

>Note: you may wish to logoff and login after making these changes.

# Containerized EOS

Install and configure Arista cEOS.

## Install the containerized EOS binary

Create an account on **www.arista.com**.

Download the software image to your laptop from the Arista software download [landing](https://www.arista.com/en/support/software-download) page. Under tab `cEOS-lab` (accept the user agreement) and download the image, in this case `cEOS-lab-4.26.8M.tar`.

Upload the image to your remote instance. For example:

**AWS**
```shell
% scp -i ~/.ssh/customer_training.pem cEOS-lab-4.26.8M.tar ubuntu@<your_instance_ip>:cEOS/
```
**Digital Ocean**
```shell
scp cEOS-lab-4.26.8M.tar root@<droplet_ip>:cEOS/
```

Log on the instance, and import the image you uploaded.

```shell
cd cEOS/
docker import cEOS-lab-4.26.8M.tar ceosimage:4.26.8M
```

Verify the image.

```shell
# docker images ceosimage
REPOSITORY   TAG       IMAGE ID       CREATED          SIZE
ceosimage    4.26.8M   aee903ad419a   20 seconds ago   1.68GB
```

## Create two EOS containers

Use the `docker create` command to expose and map ports for the containers.

```shell
docker create --expose 22 --expose 443 -p 2201:22 -p 4431:443   --name=ceos1 --privileged -e INTFTYPE=eth -e ETBA=1 -e SKIP_ZEROTOUCH_BARRIER_IN_SYSDBINIT=1 -e CEOS=1 -e EOS_PLATFORM=ceoslab -e container=docker -i -t ceosimage:4.26.8M /sbin/init systemd.setenv=INTFTYPE=eth systemd.setenv=ETBA=1 systemd.setenv=SKIP_ZEROTOUCH_BARRIER_IN_SYSDBINIT=1 systemd.setenv=CEOS=1 systemd.setenv=EOS_PLATFORM=ceoslab systemd.setenv=container=docker

docker create --expose 22 --expose 443 -p 2202:22 -p 4432:443 --name=ceos2 --privileged -e INTFTYPE=eth -e ETBA=1 -e SKIP_ZEROTOUCH_BARRIER_IN_SYSDBINIT=1 -e CEOS=1 -e EOS_PLATFORM=ceoslab -e container=docker -i -t ceosimage:4.26.8M /sbin/init systemd.setenv=INTFTYPE=eth systemd.setenv=ETBA=1 systemd.setenv=SKIP_ZEROTOUCH_BARRIER_IN_SYSDBINIT=1 systemd.setenv=CEOS=1 systemd.setenv=EOS_PLATFORM=ceoslab systemd.setenv=container=docker
```

## Create and connect networks

Create two networks and connect them to the containers.

```shell
docker network create net1
docker network create net2

docker network connect net1 ceos1
docker network connect net1 ceos2
docker network connect net2 ceos1
docker network connect net2 ceos2
```

Start the containers. Wait a few minutes for them to initialize.

```shell
docker start ceos1
docker start ceos2
```

## Initial provisioning

In real-world deployments, the network operator will rack equipment, cable and apply an initial or bootstrap configuration to enable the device to be fully configured via an automated approach. Zero-touch provisioning is an method and procedure to automate this process. In this lab environment, the following steps will be done manually.

Attach to the container. This step would normally be done via a console cable connection, here we use docker to attach to the running container(s).

```shell
docker exec -it ceos1 Cli
```

> Note: Exit out of the running container using `Ctrl + p then Ctrl + q`

Create a username and password, set the hostname, enable eAPI and LLDP.

```cli
localhost>en
localhost#config t
localhost(config)#username admin secret NetCraftsmen!123
localhost(config)#hostname ceos1
ceos1(config)#management api http-commands
ceos1(config-mgmt-api-http-cmds)#no shutdown
ceos1(config-mgmt-api-http-cmds)#ip routing
ceos1(config)#lldp run
ceos1(config)#end
eos1#copy run start
```

>Note: issue the same commands, substituting the hostname on the second router, **ceos2**.

## Enable Forwarding of LLDP frames

Linux Bridge does not forward certain Layer-2 traffic and filters some reserved multicast addresses, including LLDP frames. To [forward LLDP](https://the-bitmask.com/2017/08/04/fwd-lldp-frames-on-linuxbridge/) frames between the two containers, follow these steps on the instance running the containers.

Determine the bridge names of the docker bridged networks.

```bash
ifconfig -a | grep br-
br-6ae2a87a5a03: flags=4163<UP,BROADCAST,RUNNING,MULTICAST>  mtu 1500
br-c1adc126f1ac: flags=4163<UP,BROADCAST,RUNNING,MULTICAST>  mtu 1500
```

Modify the mask to permit forwarding LLDP frames. Substitute the bridge numbers from the `ifconfig` command above.

```bash
sudo su root
echo 16384 > /sys/class/net/br-6ae2a87a5a03/bridge/group_fwd_mask
echo 16384 > /sys/class/net/br-c1adc126f1ac/bridge/group_fwd_mask
exit
```

>Note: you can view the mask before and after, using `cat /sys/class/net/br-<number>/bridge/group_fwd_mask`.

## Verify SSH and eAPI

Verify you can access the ports 2201, 2202, 4431, 4432 on the containers:

```shell
ssh admin@127.0.0.1 -p 2201
curl https://127.0.0.1:4431/eapi  -k -vv
```

Verify the containers are forwarding LLDP traffic on the interfaces and eAPI is enabled.

```cli
 ssh admin@127.0.0.1 -p 2201
Password: 
ceos1>show lldp nei
Last table change time   : 0:13:10 ago
Number of table inserts  : 2
Number of table deletes  : 0
Number of table drops    : 0
Number of table age-outs : 0

Port          Neighbor Device ID       Neighbor Port ID    TTL 
---------- ------------------------ ---------------------- --- 
Et1           ceos2                    Ethernet1           120 
Et2           ceos2                    Ethernet2           120 

ceos1>show management http-server 
SSL Profile:        none
FIPS Mode:          No
QoS DSCP:           0
Log Level:          none
CSP Frame Ancestor: None
TLS Protocols:      1.0 1.1 1.2
   VRF           Server Status      Enabled Services 
------------- --------------------- ---------------- 
   default       HTTPS: port 443    http-commands    
```

## References

References for using Ansible with Arista EOS.
* [Ansible-EOS](https://docs.ansible.com/ansible/latest/network/user_guide/platform_eos.html)
* Enable [eAPI](https://ansible-eos.readthedocs.io/en/master/quickstart.html#option-b-connect-to-arista-node-over-eapi)


# Nautobot

Install Nautobot in a container for lab use.

## Install Nautobot lab

For this training, we are using the Nautobot lab container at `https://github.com/nautobot/nautobot-lab`.

>Note: Issue [Error on manual build of nautobot-lab](https://github.com/nautobot/nautobot-lab/issues/61)

Create a username and password for Nautobot.

```shell
export DJANGO_SUPERUSER_PASSWORD=mysecret
export DJANGO_SUPERUSER_USERNAME=admin
```

### Build the container manually
```shell
cd ~
git clone https://github.com/nautobot/nautobot-lab.git
cd nautobot-lab/
docker build -t nautobot-lab:latest .
```

> Note: due to issue [RemoteDisconnected](https://github.com/nautobot/nautobot/issues/725), a modified configuration file will need be used, `uwsgi.ini`, which is in the `misc` directory.

Create the `uwsgi.ini` file on the remote instance from `misc/uwsgi.ini`.

```shell
cd ~/cEOS
wget https://gitlab.com/joelwking/graphql4netdevops/-/raw/main/misc/uwsgi.ini
```

Run the container and configure the username and password.

```shell
docker run  -e DJANGO_SUPERUSER_PASSWORD=$DJANGO_SUPERUSER_PASSWORD  -v /home/ubuntu/nautobot-lab/uwsgi.ini:/opt/nautobot/uwsgi.ini:ro -itd --name nautobot8001 -p 8001:8000 nautobot-lab
```

Skip the following section and continue at **Create superuser account**.

### Run from Docker Hub
Running from Docker Hub may avoid `nautobot-lab/issues/61` above.

>Note: if running multiple instances of the container to support more than one student, modify the port number and name of the container.

```shell
docker run -e DJANGO_SUPERUSER_PASSWORD=$DJANGO_SUPERUSER_PASSWORD -itd --name nautobot8001 -p 8001:8000 networktocode/nautobot-lab
```
Verify

```shell
docker ps | grep nautobot
5d5ed2768e5e   networktocode/nautobot-lab   "/usr/local/bin/supe…"   22 seconds ago   Up 20 seconds (health: starting)   0.0.0.0:8000->8000/tcp, :::8000->8000/tcp
```

## Create superuser account
For both manual building or running from Docker hub, create the superuser name and password.

```shell
docker exec -it nautobot8001 nautobot-server createsuperuser --noinput --username $DJANGO_SUPERUSER_USERNAME --email $DJANGO_SUPERUSER_USERNAME@example.net
```

Connect and login using the username and password you created with your host port.

```shell
http://<your_instance_ip>:8001
```

## Create snapshot of Droplet
Create a snapshot of the Digital Ocean Droplet. Stop the containers and shutdown the system.

```shell
docker stop nautobot8001
docker stop ceos2
docker stop ceos1
sudo shutdown now -h
```
After creating the snapshot, you can create a new droplet from the [Snapshot](https://docs.digitalocean.com/products/images/snapshots/how-to/create-and-restore-droplets/)

When the snapshot process is complete, create a new droplet from the snapshot and/or re-start the original droplet.

```shell
docker start nautobot8001
docker start ceos1
docker start ceos2
```
>Note: after starting the droplet, you will need to **Enable Forwarding of LLDP frames**!

## Summary
You have successfully created two Arista switches and a Nautobot lab instance running in Docker containers.

## Author
Joel W. King @joelwking